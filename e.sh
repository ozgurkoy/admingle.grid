#!/usr/bin/expect
#/bin/sh
# Run using expect from path \
#NEED KEY INSERTED

# Get flag arguments and such
proc getopt {_argv name {_var ""} {default ""}} {
    upvar 1 $_argv argv $_var var
    set pos [lsearch -regexp $argv ^$name]
    if {$pos >= 0} {
        set to $pos
        if {$_var ne ""} {
            set var [lindex $argv [incr to]]
        }
        set argv [lreplace $argv $pos $to]
        return 1
    } else {
        if {[llength [info level 0]] == 5} {set var $default}
        return 0
    }
}

set _PASSWORD [getopt argv -p PASSWORD 0]
set _USER [getopt argv -u USER 0]
set _COMMAND [getopt argv -c COMMAND 0]
set _HOST [getopt argv -h HOST 0]
# puts $HOST
# exit
# Above line is only executed by sh
set i 0; foreach n $argv {set [incr i] $n}
set pid [ spawn -noecho ssh $USER@$HOST ]

set timeout 30
log_user 1
expect {
    "(yes/no)" {
        sleep 1
        send "yes\n"
        exp_continue
    }
    "(y/n)" {
        sleep 1
        send "y\n"
        exp_continue
    }
    -re ".+?denied.+?" {
        puts "Access not granted, aborting..."
        exit 1
    }
    -re ".+?s password.+?" { #maybe not needed
        sleep 1
        send "$PASSWORD\n"
        exp_continue
    }
    -nocase -re ".+?last login.+?" {
        send "$COMMAND\n"
        send "logout\n"
		puts "GOOD"
		exp_continue
    }
    timeout {
        puts "Timeout expired, aborting..."
        exit 1
    }
    eof {
		# puts "end"
        #puts "EOF reached."
    }
}
set status [split [wait $pid]]
set osStatus [lindex $status 2]
set procStatus [lindex $status 3]

if { $osStatus == 0 } {
    exit $procStatus
} else {
    exit $procStatus
}