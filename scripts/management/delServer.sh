#!/bin/bash
echo "Enter ip of the server you want to delete"
read ip
if ! [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
echo "ip not good"
exit
fi
if ! grep -q $ip /root/config/servers
then
echo "ip not found"
exit
fi
#remove ip
echo "Are you sure? (y/n)"
read YESNO
if [[ $YESNO == "y" ]]
then
#get type of server
SERVER=`grep $ip /root/config/servers`
REGEX="^\[([0-9\.]+)?\]\[(.+?)\]\[([a-zA-Z)])\]\[(.+?)\]"
[[ $SERVER =~ $REGEX  ]]
LABEL=${BASH_REMATCH[2]}
TYPE=${BASH_REMATCH[3]}
ZONE=${BASH_REMATCH[4]}
echo "del$TYPE|$(date --iso-8601=seconds --utc)|0|$ZONE|$ip" >> /root/config/serverOps
[[ -e /root/keys/$LABEL ]] && rm /root/keys/$LABEL
[[ -e /root/keys/$LABEL.pub ]] && rm /root/keys/$LABEL.pub
sed -i "/$ip/d" /root/config/servers
#sed "/$ip/d" /root/config/servers > /root/config/servers
fi
#process
/root/scripts/remote/updateNecessary.sh -n $ip -z $ZONE
