#!/bin/bash
#we let it sleep for random time so that they don't launch at the same time
if [[ -e "/root/NOUPDATE" ]]
then
echo "UPDATE HALTED BECAUSE OF /root/NOUPDATE flag" >> /root/update.log
exit 1
fi

#sleep $(/root/scripts/util/float.sh $RANDOM/10000+3)
ALLPARAMS=$*
DIF=10
me=`basename $0`
SCRIPT="$me $ALLPARAMS"
#echo $0 $1 $2 $3 $4 $5 $6

while getopts t:z: option
do
        case "${option}"
        in
                t) TYPE=${OPTARG};;
                z) ZONE=${OPTARG};;
	    	\?)
   		echo "Invalid option: -$OPTARG" >&2
      		exit 1
      		;;
        esac
done
if [[ $TYPE == "redirect" ]]
then
	PID=/root/pid/redirectPid
elif [[ $TYPE == "api" ]]
then
	PID=/root/pid/apiPid
elif [[ $TYPE == "union" ]]
then
        PID=/root/pid/unionPid
else
	echo "WRONG TYPE"
	exit 1
fi
#kill all other processes
/root/scripts/util/killButMe.sh "${SCRIPT}" $$
#exit
while true; do
	#get latest id another change might have occured
	currentTime=$(date +%s)
	lastChangeTime=$(<$PID)
	df=$((currentTime-lastChangeTime))
	if [[ $df -gt $DIF ]]
	then
        	echo "CHANGE BEGUN" >> /root/update.log
        	/root/scripts/management/updateServers.sh -t $TYPE -z $ZONE
	exit 1
	else
        	echo "NOT READY $lastChangeTime $currentTime" >> /root/update.log
	fi
	sleep 1
done
