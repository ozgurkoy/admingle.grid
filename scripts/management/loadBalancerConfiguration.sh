#!/bin/bash
#newConf=$(cat /root/templates/nginx.conf)
newConf=$(</root/templates/nginx.conf)
SELFIP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
IPS=()
REDS=()

while getopts i:z: option
do
        case "${option}"
        in
                i) IP=${OPTARG};;
                z) ZONE=${OPTARG};;
                :) echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
done

REGEX="^\[([0-9\.]+)?\]\[.+?\]\[([a-zA-Z]+)\]\[$ZONE\].+?$"

for line in $(cat /root/config/servers)
do
if [[ $line =~ $REGEX ]]
then
	matchIP=${BASH_REMATCH[1]}
	tag=${BASH_REMATCH[2]}
        if [ $tag == "r" ]
        then
        	IPS+=("server $matchIP;")
        elif [ $tag == "b" ]
        then
	        REDS+=($matchIP)
        fi
fi
done

#single server update
if ! [[ -z $IP ]]
then
	REDS=($IP)
fi

IPCON=${IPS[*]}
lastConf=${newConf//%REPLACEMENT%/$IPCON}

#loop now
for i in "${!REDS[@]}"
do
echo $lastConf
	echo "$lastConf"  | ssh root@${REDS[$i]} "cat > /etc/nginx/sites-enabled/default"
	ssh -t -t root@${REDS[$i]} "service nginx restart"
done
