#!/bin/bash

#if ! [ -e /root/pid/redirectPush ]
#then
#echo "[[[[[[[[[[CHANGE]]]]]]]]]]]]" >> /root/update.log

if [[ -e "/root/NOUPDATE" ]] 
then
echo "UPDATE HALTED BECAUSE OF /root/NOUPDATE flag" >> /root/update.log
exit 1
fi

if [[ $1 == "redirect" ]]
then
	echo $(date +%s) > /root/pid/redirectPid
elif [[ $1 == "api" ]]
then
	echo $(date +%s) > /root/pid/apiPid
elif [[ $1 == "union" ]]
then
	echo $(date +%s) > /root/pid/unionPid
fi

#sleep 20
#/root/scripts/management/updateRedirectServers.sh
#/root/scripts/util/removeLate.sh /root/pid/redirectPush
#else
#sleep $(/root/scripts/util/float.sh $RANDOM/10000+5)
#echo "((((((passing update)))))" >> /root/update.log
#fi
