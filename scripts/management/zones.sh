#!/bin/bash
[[ -d def ]] || { echo >&2 "Run the script from the admingle.grid folder like scripts/management/$(basename $0)"; exit 1; }

source def/vars.sh
source def/types.sh
source $UTISCP/util.sh
export MAINLOG="$LOGP/zones.log"

function finish {
	echo "Add server session finished : $(date --iso-8601=seconds --utc)">>$MAINLOG
	exit
}

trap finish INT TERM EXIT

#anchor
echo "Zone op start : $(date --iso-8601=seconds --utc)">>$MAINLOG


OPS=("ADD" "DELETE" "LIST")

readOneOfArray "Choose Operation" OPS[@] OP

if [[ $OP == "ADD" ]]
then
	until [[ $good == "1" ]];do
		inputMessage "Enter new zone" zone "^[A-Z\-\.]+"
		if grep -q "$zone" $CONP/zones
		then
			errorOutput "Zone already added"
		else
			good=1
		fi
	done;
	echo $zone >> $CONP/zones
	infoOutput "Zone added"
elif [[ $OP == "DELETE" ]]
then
	until [[ $dgood == "1" ]];do
		if [[ -f $CONP/zones ]] && [[ $(stat --printf="%s" $CONP/zones) -ge 3 ]]
		then
			ZONES=$(cat "$CONP/zones")
			infoOutput "Current Zones : \n$ZONES"
		else
			warningOutput "No zones defined yet"
			exit
		fi

		inputMessage "Type the zone name to delete" zone  "^[A-Z\-\.]+"
		if grep -q "$zone" $CONP/zones
		then
			if grep -q "\[$zone\]" $CONP/servers
			then
				errorOutput "There are servers connected to this zone, remove them to delete the zone"
				exit
			else
				sed -i "/$zone/d" $CONP/zones
				infoOutput "Zone($zone) deleted"
				dgood=1
			fi
		else
			warningOutput "Zone not found"
		fi
	done;
else
	if [[ -f $CONP/zones ]] && [[ $(stat --printf="%s" $CONP/zones) -ge 3 ]]
	then
		ZONES=$(cat "$CONP/zones")
		infoOutput "Current Zones : \n$ZONES"
	else
		warningOutput "No zones defined yet"
		exit
	fi
fi