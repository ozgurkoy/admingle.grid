#!/bin/bash
if [[ -e "$MAINPATH/NOUPDATE" ]]
then
echo "$E_OPHALTED" >> $MAINLOG
exit 1
fi

NECESFILES=("$CONP/servers")

while getopts i: option
do
        case "${option}"
        in
                i) IP=${OPTARG};;
                :) echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
done

sleep 1

for line in $(cat $CONP/servers)
do
   if [[ $line =~ $SERREG ]] #  && [[ 1 -eq 0 ]]
   then
        matchIP=${BASH_REMATCH[1]}
        matchLabel=${BASH_REMATCH[2]}

		#single server update
		if ! [[ -z $IP ]] && [[ $IP == $matchIP ]] 
		then
			echo $($UTISCP/ssh-op.sh -u "root" -h "$matchIP" -c "
			if ! grep -q \"127.0.0.1   $matchLabel\" /etc/hosts; then echo \"127.0.0.1   $matchLabel\">>/etc/hosts;fi;
			echo $matchLabel > /etc/hostname;echo $matchLabel > /proc/sys/kernel/hostname
			") >> $MAINLOG
		fi
		for i in "${!NECESFILES[@]}"
		do
			#update files
	        infoOutput "$I_PUSHINGCODE $matchIP"
			rsyncup "${NECESFILES[$i]}" "$matchIP" 
			infoOutput "$I_SUCC"

			#update firewall
			infoOutput "$I_UPDATINGFIREWALL $matchIP"
            cat $TEMPP/firewall | ssh root@$matchIP "cat > /etc/init.d/firewall" >>$MAINLOG 2>&1
			echo $($UTISCP/ssh-op.sh -u "root" -h "$matchIP" -c "
			chmod 755 /etc/init.d/firewall;
			update-rc.d firewall defaults;
			/etc/init.d/firewall restart;
			")  >>$MAINLOG 2>&1
			infoOutput "$I_SUCC"
			
			#update motd
			cat $TEMPP/motd | ssh root@$matchIP "cat > /etc/motd">>$MAINLOG 2>&1
		done
   fi
done