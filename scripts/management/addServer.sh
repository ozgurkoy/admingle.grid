#!/bin/bash
#ADMINGLE.GRID 
#Özgür Köy

#force run placement
[[ -d def ]] || { echo >&2 "Run the script from the admingle.grid folder like scripts/management/$(basename $0)"; exit 1; }

#includes
source def/vars.sh
source def/env.sh
source def/types.sh
source def/lang.sh
source $UTISCP/util.sh

#main log def
export MAINLOG="$LOGP/addServer.log"
#pid of the process
PID=$PIDP/addServer.pid

#set -o nounset
#process end
function finish {
	echo "$I_ADDSERVERENDED $(date --iso-8601=seconds --utc)">>$MAINLOG
}

trap finish EXIT

#check zones
if ! [[ -f $CONP/zones ]] || ! [[ $(stat --printf="%s" $CONP/zones) -ge 3 ]]
then
	errorOutput "$W_ZONES"
	exit
fi


#opts check
while getopts :f option
do
        case "${option}"
        in
                f) [[ -e $PID ]] && rm $PID
                ;;
        esac
done
if [[ -e $PID ]]
then
	echo "$I_FORCERUN"
	exit 1
fi


touch $PID

function deployNodeServer {
	ssh -T root@$1 "bash -s " < $REMSCP/deployNodeServer.sh $2 >>/root/update.log
}

function deployBalancerServer {
	ssh -T root@$1 "bash -s " < $REMSCP/deployLoadBalancer.sh $2 >>/root/update.log
	$MANSCP/loadBalancerConfiguration.sh -i $1 -z $3
}

#anchor
echo "$I_ADDSERVERSTART $(date --iso-8601=seconds --utc)">>$MAINLOG

#label
until [[ $good == "1" ]];do
	inputMessage "$Q_SERVERLABEL" label
	if grep -q "\[$label\]" $CONP/servers
	then
		errorOutput "$E_SERVERLABEL"
	else
		good=1
	fi
done;

#ip
until [[ $goodIP == "1" ]] && [[ $hostIsGood == "1" ]];do
	inputMessage "$Q_SERVERIP" ip
	goodIP=$(validateIP $ip)
	! [[ $goodIP == "1" ]] && errorOutput "$E_INVALIDIP"
	infoOutput "$I_WAIT"
	hostIsGood=$(portcheck $ip)
	! [[ $hostIsGood == "1" ]] && errorOutput "$Q_HOSTISNOTREACHABLE"
done;
infoOutput "$I_SUCC"

#ip-double check
if grep -q "\[$ip\]" $CONP/servers
then
	inputMessage "$Q_IPADD" yesno
	if ! [[ $yesno == "Y" ]]
	then
	warningOutput "$I_EXIT"
	exit 1
	fi
fi

#pass
until [[ $goodPass == "GOOD" ]];do
	inputMessage "$Q_ENTERPASS" password "^.+"
	infoOutput "$I_WAIT"
	goodPass=$($UTISCP/ssh-check.sh "root" "$ip" "$password")
	! [[ $goodPass == "GOOD" ]] && errorOutput "$E_INVALIDPASS"
done;
infoOutput "$I_SUCC"

#operation
readOneOfArray "$Q_CHOOSEOPERATION " GRTYPES[@] tag
mapfile ZONES < $CONP/zones

#zone
readOneOfArray "$Q_ENTERZONE" ZONES[@] zone

echo "[$ip][$label][$tag][$zone]">>$CONP/servers
echo "[$tag|$(date --iso-8601=seconds --utc)|0|$zone|$ip" >> $CONP/serverOps

infoOutput "$I_SERVERADDED"

[[ -e ~/.ssh/known_hosts ]] && ssh-keygen -R $ip >> $MAINLOG 2>&1
echo -e "StrictHostKeyChecking no\nUserKnownHostsFile /dev/null" > ~/.ssh/config


#remote ops
infoOutput "$I_UPDATINGCONFIG"
echo $($UTISCP/ssh-op.sh -p "$password" -u "root" -h "$ip" -c "
! [[ -e /root/.ssh/id_rsa ]] && ssh-keygen -t rsa -N \"\" -f /root/.ssh/id_rsa
echo -e \"Host *\nStrictHostKeyChecking no\nUserKnownHostsFile /dev/null\nLogLevel ERROR\" > /root/.ssh/config
! [[ -d /mount ]] && mkdir /mount
! [[ -d $MAINPATH ]] && mkdir $MAINPATH
! [[ -d $MAINPATH/$CONP ]] && mkdir $MAINPATH/{config,pid,log,scripts};
") >>$MAINLOG 2>&1
infoOutput "$I_SUCC"

infoOutput "$I_SSHCOPYID"
sshcopy "$ip" "$password"
infoOutput "$I_SUCC"

infoOutput "$I_GETTINGSSHKEYS"
$REMSCP/collectSSHKey.sh $ip $label
infoOutput "$I_SUCC"

infoOutput "$I_PUSHINGKEYS $ip"
$REMSCP/addSSHKeys.sh $ip
infoOutput "$I_SUCC"
infoOutput "$I_UPDATINGSSHKEYS"
$REMSCP/updateSSHKeys.sh
infoOutput "$I_SUCC"

infoOutput "$I_UPDATINGNECFILES"
$MANSCP/necessaryUpdate.sh -i $ip

exit

if [[ $tag == "r" ]]
then
	echo "Creating folders on new server. PATIENCE."
	deployNodeServer $ip $label
#	$REMSCP/nodeStartUpScripts.sh $ip redirect
	$MANSCP/updateServers.sh -t redirect -i $ip -z $zone

	echo "redirect added $ip"

elif [[ $tag == "m" ]]
then
	echo "mongo server must be initiated and added to replica set beforehand MANUALLY. "
	read monWarning
elif [[ $tag == "a" ]]
then
        echo "Creating folders on new server. PATIENCE."
	deployNodeServer $ip $label
 #       $REMSCP/nodeStartUpScripts.sh $ip api
        $MANSCP/updateServers.sh -t api -i $ip -z $zone

	echo "api added"
elif [[ $tag == "b" ]]
then
        echo "Creating folders on new server. PATIENCE."
	deployBalancerServer $ip $label $zone

        echo "balancer added"
elif [[ $tag == "u" ]]
then
        echo "Creating folders on new server. PATIENCE."
	deployNodeServer $ip $label
  #      $REMSCP/nodeStartUpScripts.sh $ip union
        $MANSCP/updateServers.sh -t union -i $ip -z $zone

        echo "union added"
fi
echo "Updating keys"
rm $PID
