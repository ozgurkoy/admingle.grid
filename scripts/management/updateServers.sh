#!/bin/bash

#script for watching over servers code and pushing them
me=`basename $0`

while getopts i:t:z: option
do
        case "${option}"
        in
                i) IP=${OPTARG};;
                z) ZONE=${OPTARG};;
                t) TYPE=${OPTARG};;
		:) echo "Option -$OPTARG requires an argument." >&2
		exit 1
      		;;
        esac

done

if [[ -z $ZONE ]]
then
	ZONE=".+?"
fi

if [[ $TYPE == "redirect" ]]
then
	EPATH=/root/src/admShortUrlServer
   	REGEX="^\[([0-9\.]+)?\]\[.+?\]\[r\]\[$ZONE\]"
elif [[ $TYPE == "api" ]]
then
	EPATH=/root/src/api
   	REGEX="^\[([0-9\.]+)?\]\[.+?\]\[a\]\[$ZONE\]"
elif [[ $TYPE == "union" ]]
then
        EPATH=/root/src/union
        REGEX="^\[([0-9\.]+)?\]\[.+?\]\[u\]\[$ZONE\]"
fi

for line in $(cat /root/config/servers)
do
   	if [[ $line =~ $REGEX ]]
   	then
		matchIP=${BASH_REMATCH[1]}
		if [[ -z $IP ]] || [[ $IP == $matchIP ]]
		then
			touch /root/pid/$TYPE.$matchIP
			echo "Updating code on $matchIP">>/root/update.log
			rsync -avzhL -e "ssh" --backup --suffix=-`date +%F-%T` $EPATH  root@$matchIP:/node/
			echo "Restarting node services on $matchIP">>/root/update.log
			ssh root@$matchIP < /root/scripts/remote/restartNode.sh >>/root/update.log
			/root/scripts/util/removeLate.sh /root/pid/$TYPE.$matchIP
		fi
   	fi
done
exit 1
