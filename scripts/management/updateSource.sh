#!/bin/bash
find /root/src/admShortUrlServer/ -type d -print0 | xargs -0 -I{} echo "{} IN_DELETE,IN_NO_LOOP,IN_CLOSE_WRITE,IN_NO_LOOP,IN_CREATE,IN_NO_LOOP /root/scripts/management/processServers.sh redirect" > /etc/incron.d/slsRedirect.conf
find /root/src/api/ -type d -print0 | xargs -0 -I{} echo "{} IN_DELETE,IN_NO_LOOP,IN_CLOSE_WRITE,IN_NO_LOOP,IN_CREATE,IN_NO_LOOP /root/scripts/management/processServers.sh api" > /etc/incron.d/slsApi.conf
find /root/src/union/ -type d -print0 | xargs -0 -I{} echo "{} IN_DELETE,IN_NO_LOOP,IN_CLOSE_WRITE,IN_NO_LOOP,IN_CREATE,IN_NO_LOOP /root/scripts/management/processServers.sh union" > /etc/incron.d/slsUnion.conf
