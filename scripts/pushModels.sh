#!/bin/bash
#bpbp
EPATH=/mount/campaigns
# api redirect and union

SELFIP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
SERVER=`grep $SELFIP /root/config/servers`
REGEXSELF="^\[[0-9\.]+\]\[.+?\]\[.+?\]\[([a-zA-Z)]+)\].+?$"
[[ $SERVER =~ $REGEXSELF  ]]
ZONE=${BASH_REMATCH[1]}

REGEX="^\[([0-9\.]+)?\].+?\[[aru]\]\[$ZONE\]"

for line in $(cat /root/config/servers)
do
   if [[ $line =~ $REGEX ]]
   then
        matchIP=${BASH_REMATCH[1]}
        if [[ -z $IP ]] || [[ $IP == $matchIP ]]
        then
                echo "Updating models on $matchIP">>/root/update.log
                rsync -avzh -e "ssh" --backup --suffix=-`date +%F-%T` $EPATH  root@$matchIP:/mount
        fi
   fi
done
