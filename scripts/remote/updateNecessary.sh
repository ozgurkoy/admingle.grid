#!/bin/bash
# TODO : collect commands and run unique ones at final
#good
if [[ -e "/root/NOUPDATE" ]]
then
echo "UPDATE HALTED BECAUSE OF /root/NOUPDATE flag" >> /root/updateneces.log
exit 1
fi

while getopts i:t:z:n: option
do
        case "${option}"
        in
                i) IP=${OPTARG};;
                t) TYPE=${OPTARG};;
                z) ZONE=${OPTARG};;
                n) IGNORE=${OPTARG};;
                :) echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
done

function sendNodeScripts {
	echo ">>>SENDING NODE SCRIPTS TO $1"
	cat /root/templates/logRotatePre.sh | ssh root@$1 "cat > /root/scripts/logRotatePre.sh"
	cat /root/templates/logRotatePost.sh | ssh root@$1 "cat > /root/scripts/logRotatePost.sh"
	cat /root/templates/logrotate.conf | ssh root@$1 "cat > /root/conf/logrotate.conf"
	cat /root/templates/logRotateExec.sh | ssh root@$1 "cat > /etc/cron.daily/ADMlogrotate"

	ssh root@$1 "chmod 755 /etc/cron.daily/ADMlogrotate"
	ssh root@$1 "chmod +x /root/scripts/logRotatePre.sh"
	ssh root@$1 "chmod +x /root/scripts/logRotatePost.sh"

	cat /root/templates/pullModels.sh | ssh root@$1 "cat > /root/scripts/pullModels.sh"
	cat /root/templates/pushModels.sh | ssh root@$1 "cat > /root/scripts/pushModels.sh"

	ssh root@$1 "chmod +x /root/scripts/pullModels.sh;"
	ssh root@$1 "chmod +x /root/scripts/pushModels.sh"
}

sleep 2

echo "UPDATE NECESSARY RAN@@"  >> /root/updateneces.log

NECES=("/root/config/servers")
REGEX="^\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]"
SELFIP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`

LASTSERVER=$(tail -1 /root/config/servers | head -1)
[[ $LASTSERVER =~ $REGEX  ]]
LASTSERVER=${BASH_REMATCH[1]}

REGEX="^\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]"

for line in $(cat /root/config/servers)
do
   if [[ $line =~ $REGEX ]] #  && [[ 1 -eq 0 ]]
   then
        matchIP=${BASH_REMATCH[1]}
        matchLabel=${BASH_REMATCH[2]}
        matchType=${BASH_REMATCH[3]}
        zone=${BASH_REMATCH[4]}

	#no ip, force type or not
        if ( [[ -z $IP ]] || [[ $IP == $matchIP ]] ) && ( [[ -z $TYPE ]] || [[ $matchType =~ [$TYPE]  ]] )
        then
		echo "$matchIP Types: $TYPE   <<< HERE LAN"

		if [ $matchType == "a" ] && ( [[ -z $IGNORE ]] || [[ $IGNORE != $matchIP ]] )
		then
			echo "----------Update API Necessity" >> /root/updateneces.log
			/root/scripts/remote/nodeStartUpScripts.sh $matchIP api
			sendNodeScripts $matchIP
		elif [ $matchType == "r" ] && ( [[ -z $IGNORE ]] || [[ $IGNORE != $matchIP ]] )
		then
			echo "----------Update Redirect Necessity" >> /root/updateneces.log
			/root/scripts/remote/nodeStartUpScripts.sh $matchIP redirect
			sendNodeScripts $matchIP
		elif [ $matchType == "u" ] && ( [[ -z $IGNORE ]] || [[ $IGNORE != $matchIP ]] )
		then
			echo "----------Update Union Necessity" >> /root/updateneces.log
			/root/scripts/remote/nodeStartUpScripts.sh $matchIP union
			sendNodeScripts $matchIP
		elif [ $matchType == "b" ] && ( [[ -z $IGNORE ]] || [[ $IGNORE != $matchIP ]] )
		then
			echo "----------Update Balancer Necessity" >> /root/updateneces.log
			/root/scripts/management/loadBalancerConfiguration.sh -i $matchIP -z $zone
		fi

		#single server update
		if ! [[ -z $IP ]]
		then
			ssh root@$matchIP "if ! grep -q \"127.0.0.1   $matchLabel\" /etc/hosts; then echo \"127.0.0.1   $matchLabel\">>/etc/hosts;fi;"
			#instant change
			ssh root@$matchIP "echo $matchLabel > /etc/hostname;echo $matchLabel > /proc/sys/kernel/hostname"
		fi
        fi

	for i in "${!NECES[@]}"
	do
		#create necessary tables
		#ssh root@$matchIP "! [[ -d /mount ]] && mkdir /mount;! [[ -d /root/config ]] && mkdir /root/{config,pid,log,scripts};"

		#update files
	        echo "Updating necessary files on $matchIP ${NECES[$i]} root@$matchIP:${NECES[$i]}" >> /root/updateneces.log
	        rsync -avzh -e "ssh" --backup --suffix=-`date +%F-%T` ${NECES[$i]}  root@$matchIP:${NECES[$i]} >> /root/updateneces.log

		#update firewall
		echo ">> UPDATING FIREWALL ON $matchIP"
                cat /root/templates/firewall | ssh root@$matchIP "cat > /etc/init.d/firewall"
		ssh root@$matchIP "chmod 755 /etc/init.d/firewall;update-rc.d firewall defaults;/etc/init.d/firewall restart;"
		echo ">> UPDATING MOTD ON $matchIP"
		cat /root/templates/motd | ssh root@$matchIP "cat > /etc/motd"
	done
   fi
done
#exit
#LASTOP=$(tail -1 /root/config/serverOps | head -1)

for LINE in $(cat /root/config/serverOps)
do
	PARTS=(${LINE//\|/ })
	LASTOP=${PARTS[0]}
	DATE=${PARTS[1]}
	STATUS=${PARTS[2]}
	EZONE=${PARTS[3]}
	EIP=${PARTS[4]}
	if [[ $STATUS == 0 ]] && ( [[ -z $ZONE ]] || [[ $EZONE =~ $ZONE ]] )
	then
		echo "--- THIS IS NOT PROCESSED $LINE" >> /root/updateneces.log
		NEWDATE=$(date --iso-8601=seconds --utc);
		#mark as done
		sed -i".bak" "s/^$LINE\$/$LASTOP|$DATE|1|$EZONE|$EIP|$NEWDATE/g" /root/config/serverOps

		#special ops
		if [[ $LASTOP == "r" ]]
		then
			echo "Updating redirect server"  >> /root/updateneces.log

        		ssh root@$EIP "/root/scripts/pullModels.sh"

			#update api servers
			/root/scripts/management/syncCode.sh -t api -z $EZONE  #update api

			#nginx
			echo "Updating load balancers"  >> /root/updateneces.log
			echo "Updating load balancers"
			/root/scripts/management/loadBalancerConfiguration.sh -z $EZONE

		elif [[ $LASTOP == "u" ]]
		then
			echo "Updating union server"  >> /root/updateneces.log

        		ssh root@$EIP "/root/scripts/pullModels.sh"

			#update union servers
			/root/scripts/management/syncCode.sh -t union -z $EZONE

		elif [[ $LASTOP == "delr" ]]
		then
			echo "deleted redirection" >> /root/updateneces.log

			#update api
        		/root/scripts/management/syncCode.sh -t api -z $EZONE

			#reload balancer configuration
			/root/scripts/management/loadBalancerConfiguration.sh -z $EZONE
		elif [[ $LASTOP == "a" ]]
		then
			#upload model file watcher
			echo "Updating api server"  >> /root/updateneces.log
			#create scripts
			cat /root/templates/pullModels.sh | ssh root@$EIP "cat > /root/scripts/pullModels.sh"

			#flush incron updater, for pull then pull models
        		ssh root@$EIP "mkdir /etc/incron.d/;mkdir /etc/incron.d;cat /dev/null > /etc/incron.d/apiModel.conf;sleep 1;/root/scripts/pullModels.sh;touch /etc/incron.d/apiModel.conf"

			#initiate incron updater
			#cat /root/templates/apiModelIncon.conf | ssh root@$EIP "cat > /etc/incron.d/apiModel.conf"
		elif [[ $LASTOP == "m" ]]
		then
			echo "Restarting node instances due to mongo server addition"  >> /root/updateneces.log
			/root/scripts/management/syncCode.sh -t api -z $EZONE
			/root/scripts/management/syncCode.sh -t redirect  -z $EZONE
			/root/scripts/management/syncCode.sh -t union -z $EZONE
		elif [[ $LASTOP == "delm" ]]
		then
			echo "Restarting node instances due to mongo server removal"  >> /root/updateneces.log
			/root/scripts/management/syncCode.sh -t api -z $EZONE
			/root/scripts/management/syncCode.sh -t redirect -z $EZONE
			/root/scripts/management/syncCode.sh -t union -z $EZONE
		fi

	fi
done
