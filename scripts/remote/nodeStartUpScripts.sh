#!/bin/bash
ssh -t -t root@$1 "[ -d /node ] || mkdir /node"
ssh -t -t root@$1 "[ -d /root/pid ] || mkdir /root/pid"
ssh -t -t root@$1 "[ -d /root/log ] || mkdir /root/log"
ssh -t -t root@$1 "[ -d /root/conf ] || mkdir /root/conf"
ssh -t -t root@$1 "[ -d /root/scripts ] || mkdir /root/scripts"

cat /root/templates/forever |ssh root@$1 "cat > /etc/init.d/forever"
if [[ $2 == "api" ]]
then
	echo "API"
        echo "Installing model file watcher"  >> /root/update.log
        cat /root/templates/apiModelIncon.conf | ssh root@$1 "cat > /etc/incron.d/apiModel.conf"
elif [[ $2 == "union" ]]
then
echo "union"
elif [[ $2 == "redirect" ]]
then
echo "redirect"

fi
ssh -t -t root@$1 "chmod 755 /etc/init.d/forever"
ssh root@$1 "update-rc.d forever defaults"

