#!/bin/bash

for line in $(cat $CONP/servers)
do
	if [[ $line =~ $SERREG ]]
	then
		matchIP=${BASH_REMATCH[1]}
		
		infoOutput "$I_UPDATINGSSHKEYS $matchIP"
		echo $($UTISCP/ssh-op.sh -u "root" -h "$matchIP" -c "
		[[ -e /root/.ssh/known_hosts ]] && sed -i\".bak\" \"/^$matchIP/d\" /root/.ssh/known_hosts
		") >>$MAINLOG 2>&1
		$REMSCP/addSSHKeys.sh $matchIP
   fi
done
