#!/bin/bash
for line in $(cat $CONP/servers)
do
	if [[ $line =~ $SERREG ]]
	then
       	matchIP=${BASH_REMATCH[1]}
       	matchLabel=${BASH_REMATCH[2]}

		if [[ -e $KEYSP/$matchLabel ]]
		then
			infoOutput "Pushing to $1 the key $matchLabel"
			ssh-copy-id -i $KEYSP/$matchLabel -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null root@$1 >>$MAINLOG 2>&1
		fi
	fi
done
exit 1

FILES=$KEYSP/*
for f in $FILES
do
	EXT=${f:(-4)}
	if [[ $EXT == ".pub" ]]
	then
	KEY=${f:0:(-4)}
	
	echo "pushing to $1 the key $f" >> $MAINLOG
	
	ssh-copy-id -i $f root@$1 >> $MAINLOG
	sleep 1
	fi
done
