#!/bin/bash
export DEBIAN_FRONTEND="noninteractive"

dpkg --configure -a
apt-get -y -q update >> /var/log/postInstall.log
apt-get install -y -q build-essential software-properties-common curl libgeoip-dev >> /var/log/postInstall.log;
apt-get install -y -q python-software-properties  >> /var/log/postInstall.log;
apt-get -y -q update  >> /var/log/postInstall.log;
apt-get -y -q upgrade  >> /var/log/postInstall.log;
apt-get -y -q install nginx  >> /var/log/postInstall.log;

