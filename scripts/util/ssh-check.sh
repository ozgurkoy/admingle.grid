#!/bin/sh
# Run using expect from path \
exec expect -f "$0" "$@"

# Above line is only executed by sh
set i 0; foreach n $argv {set [incr i] $n}
set pid [ spawn -noecho ssh $1@$2 ]
set timeout 30
log_user 0
expect {
    "(yes/no)" {
        sleep 1
        send "yes\n"
        exp_continue
    }
    "(y/n)" {
        sleep 1
        send "y\n"
        exp_continue
    }
    -re ".+?denied.+?" {
        puts "Access not granted, aborting..."
        exit 1
    }
    -re ".+?s password.+?" {
        sleep 1
        send "$3\n"
        exp_continue
    }
    -nocase -re ".+?last login.+?" {
        send "who\n"
        send "logout\n"
		puts "GOOD"
		exp_continue
    }
    timeout {
        puts "Timeout expired, aborting..."
        exit 1
    }
    eof {
		# puts "end"
        #puts "EOF reached."
    }
}
set status [split [wait $pid]]
set osStatus [lindex $status 2]
set procStatus [lindex $status 3]

if { $osStatus == 0 } {
    exit $procStatus
} else {
    exit $procStatus
}