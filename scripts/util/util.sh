array_hasValue () {
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
		element=${element//$'\n'/} #remove new lines coming from read file
        if [[ $element == $seeking ]]; then
            in=0
				echo $in
            break
        fi
    done
    return $in
}

inputMessage () {
    local result=$2
	local MSG=$1
	local __v=""
	if [[ -z $3 ]]; then
		REGEX="^[a-zA-Z0-9\_\.\-]+$"
	else
		REGEX=$3
	fi
		
	MSG=$(printf "$1")
	MSG="$(tput setaf 10)[INPUT] ${MSG} $(tput sgr0)"
	! [[ -z "$MAINLOG" ]] && echo "
	$MSG
	" >> "$MAINLOG"
	errorRun=0
	
	until [[ ${#__v} -gt 2 ]] && [[ $__v =~ $REGEX ]] ;do
		echo "$MSG"
		
		if [[ $errorRun != "0" ]]
		then
			infoOutput "Use alpha numeric characters, \nhere is the regex checking your input for your pleasure : $REGEX"	
		fi
		
		__v=$(getInput)
		# __v=$__v | sed 's/\s$//'
		errorRun=1
	done

    eval $result="'$__v'"
}

rsyncup () {
    rsync -avzh -e "ssh" --backup --suffix=-`date +%F-%T` $1  root@$2:$1 >>$MAINLOG 2>&1
}
export -f rsyncup
sshcopy () {
	echo $($UTISCP/ssh-copy.sh "root" "$1" "$2") >> $MAINLOG
	# ssh-copy-id -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null root@$1 >> $MAINLOG
}

createRemoteFolders () {
	ssh root@$1 "! [[ -d /mount ]] && mkdir /mount;! [[ -d $CONP ]] && mkdir /root/{config,pid,log,scripts};"
}

portcheck() {
	if ! nc -w 5 -z $1 22 2>/dev/null 
	then
		echo "0"
	else
		echo "1"
		# exit
	fi
}

getInput () {	
	read -e something

	! [[ -z "$MAINLOG" ]] && echo "
	READ: $something
	" >> $MAINLOG
	echo $something
}

errorOutput () {
	MSG=$(printf "$1")
	
	echo "[ERROR] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 1)[ERROR] ${MSG}$(tput sgr0)"
	
}

warningOutput () {
	MSG=$(printf "$1")
	
	echo "[WARNING] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 3)[WARNING] ${MSG}$(tput sgr0)"
}

infoOutput () {
	MSG=$(printf "$1")
	echo "[INFO] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 4)[INFO] ${MSG}$(tput sgr0)"
}

export -f infoOutput

validateIP () {
	[[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] && echo "1"
}

getArray() {
    i=0
    while read line # Read a line
    do
        array[i]=$line # Put it into the array
        i=$(($i + 1))
    done < $1
}

readOneOfArray () {
    declare -a _OP=("${!2}")
	local good=0
    local result=$3
	history -c
	until [[ $good == "1" ]];do
		TYPEQ="$1 \n(Type one of the below, You can use TAB)\n"

		for i in "${!_OP[@]}"
		do
			ebe=$__v | sed 's/[^a-zA-Z0-9]//g'
			
			_T="${_OP[$i]}"
			_T=${_T//$'\n'/} #remove new lines coming from read file
		    history -s $_T
			
			TYPEQ+="-> $_T\n"
			
		done

		inputMessage "$TYPEQ " OP
	
		[[ $(array_hasValue _OP $OP) ]] && good="1"
	done
	
    eval $result="'$OP'"
}

getAssociateArray () {	
	assoc_array_string=$(declare -p $1)
	eval "declare -gA "$2"="${assoc_array_string#*=}
}
