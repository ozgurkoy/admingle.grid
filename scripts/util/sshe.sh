#!/usr/bin/expect -f

#Expect script to supply root/admin password for remote ssh server
#and execute command.
#This script needs three argument to(s) connect to remote server:
#password = Password of remote UNIX server, for root user.
#ipaddr = IP Addreess of remote UNIX server, no hostname
#scriptname = Path to remote script which will execute on remote server
#For example:
#./sshlogin.exp password 192.168.1.11 who
#------------------------------------------------------------------------
#Copyright (c) 2004 nixCraft project <http://cyberciti.biz/fb/>
#This script is licensed under GNU GPL version 2.0 or above
#-------------------------------------------------------------------------
#This script is part of nixCraft shell script collection (NSSC)
#Visit http://bash.cyberciti.biz/ for more information.
#----------------------------------------------------------------------
#set Variables

set ipaddr [lrange $argv 0 0]
   set password [lrange $argv 1 1]
   set scriptname [lrange $argv 2 2]
   set arg1 [lrange $argv 3 3]
   set arg2 [lrange $argv 4 4]
   set arg3 [lrange $argv 5 5]
   set arg4 [lrange $argv 6 6]
   set arg5 [lrange $argv 7 7]
   set arg6 [lrange $argv 8 8]
   set arg7 [lrange $argv 9 9]
   
   
   # puts $password
   # exit


#setting a timeout for the password prompt 5 seconds larger than the SSH ConnectionTimeout parameter

set timeout 35

#now connect to remote UNIX box (ipaddr) with given script to execute

set pid [spawn -noecho ssh -o "ConnectTimeout 30" -o "CheckHostIP no" -o "StrictHostKeyChecking no" $ipaddr $scriptname $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7]
match_max 5000

#look for password prompt

log_user 1

   expect {
      "denied"                       {puts "CRITICAL: wrong SSH password" ; exit 2}
      "Name or service not known"    {puts "CRITICAL: cannot resolve SSH server name $ipaddr" ; exit 2}
      "Connection refused"           {puts "CRITICAL: SSH connection to $ipaddr refused" ; exit 2}
      "Connection timed out"         {puts "CRITICAL: SSH connection to $ipaddr timed out" ; exit 2}
      timeout                        {puts "CRITICAL: SSH server timed out while prompting for password" ; exit 2}
      "?assword:"
   }

# send password
# puts "hadi"
send -- "$password"

# send blank line to make sure we get back to gui

send -- "r"
   expect "r"
   log_user 1

# now we wait up to 30 seconds

set timeout 30
   expect {
       timeout                   {puts "CRITICAL: execution of $scriptname timed out after 30 seconds" ; exit 2}
       eof
   }

set waitret [wait]
   catch {close}

set state [lindex $waitret 2]
   exit [lindex $waitret 3
