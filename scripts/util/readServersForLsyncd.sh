#!/bin/bash
REGEX="^\[([0-9\.]+)?\].+?\[[$1]\]"
SELFIP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
for line in $(cat /root/config/servers)
do
   if [[ $line =~ $REGEX ]]
   then
        matchIP=${BASH_REMATCH[1]}
	if ! [[ $matchIP == $SELFIP  ]]
	then
		echo "$matchIP:/$2/"
	fi
   fi
done
