#!/bin/bash

set -o emacs
bind 'set show-all-if-ambiguous on'
bind 'set completion-ignore-case on'

COMP_WORDBREAKS=${COMP_WORDBREAKS//:}

#bind TAB:menu-complete
bind 'TAB:dynamic-complete-history'
