export Q_IPADD="Ip already added, Do you want to add anyway? \nIf the server is removed the ALL services on this machine will be useless (Y/N)"
export Q_SERVERLABEL="Enter server label"
export Q_SERVERIP="Enter server ip"
export Q_CHOOSEOPERATION="Choose operation : "
export Q_ENTERZONE="Enter zone. this can be production, test or anything. And be careful."
export Q_HOSTISNOTREACHABLE="Host is not reachable"
export Q_ENTERPASS="Enter password of the server"
export I_FORCERUN="Last execution was broken or halted. Start with -f to override"
export I_ADDSERVERSTART="Add server session start : "
export I_ADDSERVERENDED="Add server session finished : "
export I_SERVERADDED="Server added to configuration"
export I_SSHCOPYID="Now processing with ssh-copy-id"
export I_GETTINGSSHKEYS="Getting ssh-key from server"
export I_PUSHINGKEYS="Pushing keys to server"
export I_UPDATINGSSHKEYS="Updating all servers with SSH keys"
export I_UPDATINGCONFIG="Updating configuration on server"
export I_UPDATINGNECFILES="Uploading necessary files"
export I_UPDATINGSSHKEYS="Updating ssh keys"
export I_EXIT="exiting..."
export I_SUCC="Successful"
export I_WAIT="Please wait"
export I_UPDATINGFIREWALL="Updating Firewall on"
export I_PUSHINGCODE="Pushing code to:"
export W_ZONES="You should define some zones to start. Use scripts/management/zones.sh script"
export E_SERVERLABEL="Label already added"
export E_INVALIDIP="Ip not good"
export E_INVALIDPASS="Password not good"
export E_OPHALTED="UPDATE HALTED BECAUSE OF $MAINPATH/NOUPDATE flag"